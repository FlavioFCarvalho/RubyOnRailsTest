describe 'Matchers igualdade' do
	it '#equal - Teste se é o mesmo objeto' do
		x = "ruby"
		y = "ruby"
		expect(x).not_to equal(y)
		expect(x).to equal(x)
	end

	it '#be - Teste se é o mesmo objeto' do
		x = "ruby"
		y = "ruby"
		expect(x).not_to equal(y)
		expect(x).to equal(x)
	end

	it '#eql - Teste o valor/conteúdo' do
		x = "ruby"
		y = "ruby"
		expect(x).to eql(y)
	end

	it '#eq - Teste o valor/conteúdo *eq é alias do eql (um atalho)' do
		x = "ruby"
		y = "ruby"
		expect(x).to eq(y)
	end

end