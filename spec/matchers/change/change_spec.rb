require 'contador'

describe 'Matcher change' do
	it {expect {Contador.incrementa}.to change {Contador.qtd}} #qtd1
	it {expect {Contador.incrementa}.to change {Contador.qtd}.by(1)} #qtd2
	it {expect {Contador.incrementa}.to change {Contador.qtd}.from(2).to(3)}#qtd3
end
