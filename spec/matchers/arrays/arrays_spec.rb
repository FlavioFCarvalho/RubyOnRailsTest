RSpec::Matchers.define_negated_matcher :exclude, :include 

describe Array.new([1,2,3]), "Array",type: 'collection'  do

	#precisca passar apenas um elemento contido no array
	it '#include' do
		expect(subject).to include(2)
		expect(subject).to include(2,1)
	end

	it{expect(subject).to exclude(4)}
    
    #precisa passar todos o elementos contidos no array
	it '#contain_exactly', :slow do
		expect(subject).to contain_exactly(2,1,3)
	end
    
    #Precisa passar o array com []
	it '#match_array' do
		expect(subject).to match_array([2,1,3])
	end
end